public class Maskify {
    public static String maskify(String str) {
        if (str.length() < 4) {
            return str;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < str.length() - 4; i++) {
            stringBuilder.append("#");
        }
        stringBuilder.append(str.substring(str.length() - 4));
        return stringBuilder.toString();
    }
}
